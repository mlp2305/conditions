package conditions

import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.junit.Test
import org.mikkelp.conditions.HSQLDBMemory
import org.junit.Assert
@RunWith(classOf[JUnit4])
class TestProperties {
	@Test 
	def testGetSet() {
		val j = new HSQLDBMemory("testValues");
		j.setProperty("test", "this is a test")
		val value = j.getProperty("test")
		println(value)
		Assert.assertEquals("this is a test", value)
	}
}
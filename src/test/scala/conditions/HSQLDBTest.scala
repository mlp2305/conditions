package conditions
import java.io.IOException
import java.io.PrintWriter
import java.sql.Connection
import java.sql.DriverManager
import java.sql.SQLException
import org.hsqldb.server.Server
import org.junit.Test
import org.junit.Ignore
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mikkelp.conditions.Condition
import org.mikkelp.conditions.ConditionDatabase
import org.mikkelp.conditions.HSQLDBMemory
import org.hamcrest.core.IsEqual
import org.mikkelp.conditions.HSQLDBMemory

@RunWith(classOf[JUnit4])
class HSQLDBTest {
	def getDatabase() = {
	  
	}
    @Test
    def testCreateDatabase() {
	  val j = new HSQLDBMemory("test1");
	  
	}
    @Test
    def testInsert() {
      val j = new HSQLDBMemory("test2");
	  val condition = new Condition()
	  j.insertCondition(condition)
      
    }
    @Test
    def testGetByNameAndTestValues() {
      val j = new HSQLDBMemory("test3");
	  val condition = new Condition()
	  condition.name="testName"
	  condition.success = true
	  condition.count = 11
	  condition.var1 = 1
	  condition.var2 = 2
	  condition.var3 = 3
	  condition.var4 = 4
	  condition.var5 = 5
	  condition.var6 = 6
	  condition.var7 = 7
	  condition.var8 = 8
	  condition.var9 = 9
	  condition.var10 = 10
	  condition.var11 = 11
	  condition.var12 = 12
	  condition.var13 = 13
	  condition.var14 = 14
	  condition.var15 = 15
	  condition.var16 = 16
	  condition.var17 = 17
	  condition.var18 = 18
	  condition.var19 = 19
	  condition.var20 = 20
	  
	  j.insertCondition(condition)
      val c = j.selectWithName("testName")(0)
      assert(c.isEqual(condition))
      assert(c.var1==condition.var1)
      assert(c.var2==condition.var2)
      assert(c.var3==condition.var3)
      assert(c.var4==condition.var4)
      assert(c.var5==condition.var5)
      assert(c.var6==condition.var6)
      assert(c.var7==condition.var7)
      assert(c.var8==condition.var8)
      assert(c.var9==condition.var9)
      assert(c.var10==condition.var10)
      assert(c.var11==condition.var11)
      assert(c.var12==condition.var12)
      assert(c.var13==condition.var13)
      assert(c.var14==condition.var14)
      assert(c.var15==condition.var15)
      assert(c.var16==condition.var16)
      assert(c.var17==condition.var17)
      assert(c.var18==condition.var18)
      assert(c.var19==condition.var19)
      assert(c.var20==condition.var20)
      
      
    }
    @Test
    def testCategorize() {
      val j = new HSQLDBMemory("test4");
	  var condition1 = new Condition()
	  condition1.setSucces(true)
	  condition1.name = "name1"
	  condition1.var1 = 1f
	  condition1.var2 = 1
	  condition1.var3 = 1f
	  condition1.var4 = 1f
	  condition1.var5 = 1f
	  condition1.var6 = 1f
	  condition1.var7 = 1f
	  condition1.var8 = 1f
	  condition1.var9 = 1f
	  condition1.var10 =1f
	  condition1.var11 = 1f
	  condition1.var12 =1f
	  
	  j.insertCondition(condition1)
	  var condition2 = new Condition()
	  condition2.name = "name2"
	  condition2.setSucces(true)
	  condition2.var1 = 100f
	  condition2.var2 = 100f
	  condition2.var3 = 100f
	  condition2.var4 = 100f
	  condition2.var5 = 100f
	  condition2.var6 = 100f
	  condition2.var7 = 100f
	  condition2.var8 = 100f
	  condition2.var9 = 100f
	  condition2.var10 = 100f
	  condition2.var11 = 100f
	  condition2.var12 = 100f
	  j.insertCondition(condition2)
	  assert(j.size()==2)
	  var categorizeBy = new Condition();
	  
	  categorizeBy.var1 = 10
	  categorizeBy.var2 = 10
	  categorizeBy.var3 = 10
	  categorizeBy.var4 = 10
	  categorizeBy.var5 = 10
	  categorizeBy.var6 = 10
	  categorizeBy.var7 = 10
	  categorizeBy.var8 = 10
	  categorizeBy.var9 = 10
	  categorizeBy.var10 = 10
	  categorizeBy.var11 = 10
	  categorizeBy.var12 = 10
	  j.categorizeBy(categorizeBy)
	  condition1 = j.selectWithName("name1")(0)
	  condition2 = j.selectWithName("name2")(0)
	  //println(condition1)
	  //println(condition1)
	  assert(condition1.var1==0)
	  assert(condition1.var2==0)
	  assert(condition1.var3==0)
	  assert(condition1.var4==0)
	  assert(condition1.var5==0)
	  assert(condition1.var6==0)
	  assert(condition1.var7==0)
	  assert(condition1.var8==0)
	  assert(condition1.var9==0)
	  assert(condition1.var10==0)
	  assert(condition1.var11==0)
	  assert(condition1.var12==0)
	  
	  assert(condition2.var1==10)
	  assert(condition2.var2==10)
	  assert(condition2.var3==10)
	  assert(condition2.var4==10)
	  assert(condition2.var5==10)
	  assert(condition2.var6==10)
	  assert(condition2.var7==10)
	  assert(condition2.var8==10)
	  assert(condition2.var9==10)
	  assert(condition2.var10==10)
	  assert(condition2.var11==10)
	  assert(condition2.var12==10)
	  var min = j.getMinValues
	  var max = j.getMaxValues
	  assert(min.var1==1)
	  assert(min.var2==1)
	  assert(min.var3==1)
	  assert(min.var4==1)
	  assert(min.var5==1)
	  assert(min.var6==1)
	  assert(min.var7==1)
	  assert(min.var8==1)
	  assert(min.var9==1)
	  assert(min.var10==1)
	  assert(min.var11==1)
	  assert(min.var12==1)
	  assert(max.var1==100)
	  assert(max.var2==100)
	  assert(max.var3==100)
	  assert(max.var4==100)
	  assert(max.var5==100)
	  assert(max.var6==100)
	  assert(max.var7==100)
	  assert(max.var8==100)
	  assert(max.var9==100)
	  assert(max.var10==100)
	  assert(max.var11==100)
	  assert(max.var12==100)
	  assert(j.getCategorizedBy.isEqual(categorizeBy))
	  var c = new Condition()
	  c.var1 = 100
	  c.var2 = 100
	  c.var3 = 100
	  c.var4 = 100
	  c.var5 = 100
	  c.var6 = 100
	  c.var7 = 100
	  c.var8 = 100
	  c.var9 = 100
	  c.var10 = 100
	  c.var11 = 100
	  c.var12 = 100
	  j.categorizeCondition(c)
	  assert(c.var1==10)
	  assert(c.var2==10)
	  assert(c.var3==10)
	  assert(c.var4==10)
	  assert(c.var5==10)
	  assert(c.var6==10)
	  assert(c.var7==10)
	  assert(c.var8==10)
	  assert(c.var9==10)
	  assert(c.var10==10)
	  assert(c.var11==10)
	  assert(c.var12==10)
	  
	  
	  
	  
	}
    
    @Test
    def testLookups() {
      val j = new HSQLDBMemory("test5");
      val condition = new Condition()
	  condition.name="testName"
	  condition.success = true
	  condition.count = 11
	  condition.var1 = 1
	  condition.var2 = 2
	  condition.var3 = 3
	  condition.var4 = 4
	  condition.var5 = 5
	  condition.var6 = 6
	  condition.var7 = 7
	  condition.var8 = 8
	  condition.var9 = 9
	  condition.var10 = 10
	  condition.var11 = 11
	  condition.var12 = 12
	  condition.var13 = 13
	  condition.var14 = 14
	  condition.var15 = 15
	  condition.var16 = 16
	  condition.var17 = 17
	  condition.var18 = 18
	  condition.var19 = 19
	  condition.var20 = 20
	  j.insertCondition(condition)
	  
	  var c = j.selectWhenEquals(condition)(0)
	  assert(c.isEqual(condition))
	  condition.name="differentName"
	  var list = j.selectWhenEquals(condition)
	  assert(list.size==0)
	  var c2 =j.selectWhenSameState(condition,1)(0)
	  condition.name="testName"
	  assert(c2.isEqual(condition))
	}
    @Test
    def testCountNumberConditions() {
      val j = new HSQLDBMemory("test6");
	  val condition = new Condition()
	  condition.name="testName"
	  condition.success = true
	  condition.count = 11
	  j.insertCondition(condition)
	  j.insertCondition(condition)
	  j.insertCondition(condition)
	  j.insertCondition(condition)
	  condition.name="differentName"
	  j.insertCondition(condition)  
	  assert(j.getNumberOfConditionWithSameState(condition)==5)
	  condition.name="testName"
	  var size = j.getNumberOfConditionThatEquals(condition) 
	  assert(size==4)
	}
    @Test
    def testZip() {
      val j = new HSQLDBMemory("test7");
	  val condition = new Condition()
	  condition.name="testName"
	  condition.success = true
	  condition.count = 11
	  
	  j.insertCondition(condition)
	  j.insertCondition(condition)
	  j.insertCondition(condition)
	  condition.setSucces(false)
	  j.insertCondition(condition)
	  j.zip;
	  var size = j.size
	  assert(size==1)
	  var condition2 = j.selectAll()(0)
	  assert(condition.isEqual(condition2))
	  assert(condition2.count==4)
	  assert(condition2.successCount==3)
	}
}



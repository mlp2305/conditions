package conditions
import java.io.IOException
import java.io.PrintWriter
import java.sql.Connection
import java.sql.DriverManager
import java.sql.SQLException
import org.hsqldb.server.Server
import org.junit.Test
import org.junit.Ignore
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mikkelp.conditions.Condition
import org.mikkelp.conditions.ConditionDatabase
import org.mikkelp.conditions.HSQLDBMemory
import org.hamcrest.core.IsEqual
import org.mikkelp.conditions.HSQLDBMemory

@RunWith(classOf[JUnit4])
class TestCategorizeSmallValues {
	@Test
    def testCategorizeWithSmallValues() {
      val j = new HSQLDBMemory("smallValues");
	  var condition1 = new Condition()
	  condition1.name = "name1"
	  condition1.var1 = 0.1f
	  condition1.var2 = 0.1f
	  condition1.var3 = 0.1f
	  condition1.var4 = 0.1f
	  condition1.var5 = 0.1f
	  condition1.var6 = 0.1f
	  condition1.var7 = 0.1f
	  condition1.var8 = 0.1f
	  condition1.var9 = 0.1f
	  condition1.var10 = 0.1f
	  condition1.var11 = 0.1f
	  condition1.var12 = 0.1f
	  condition1.var13 = 0.1f
	  condition1.var14 = 0.1f
	  condition1.var15 = 0.1f
	  condition1.var16 = 0.1f
	  condition1.var17 = 0.1f
	  condition1.var18 = 0.1f
	  condition1.var19 = 0.1f
	  condition1.var20 = 0.1f
	  
	  j.insertCondition(condition1)
	  var condition2 = new Condition()
	  condition2.name = "name2"
	  condition2.var1 = 0.2f
	  condition2.var2 = 0.2f
	  condition2.var3 = 0.2f
	  condition2.var4 = 0.2f
	  condition2.var5 = 0.2f
	  condition2.var6 = 0.2f
	  condition2.var7 = 0.2f
	  condition2.var8 = 0.2f
	  condition2.var9 = 0.2f
	  condition2.var10 = 0.2f
	  condition2.var11 = 0.2f
	  condition2.var12 = 0.2f
	  condition2.var13 = 0.2f
	  condition2.var14 = 0.2f
	  condition2.var15 = 0.2f
	  condition2.var16 = 0.2f
	  condition2.var17 = 0.2f
	  condition2.var18 = 0.2f
	  condition2.var19 = 0.2f
	  condition2.var20 = 0.2f
	  
	  j.insertCondition(condition2)
	  assert(j.size()==2)
	  var categorizeBy = new Condition();
	  categorizeBy.var1 = 10
	  categorizeBy.var2 = 10
	  categorizeBy.var3 = 10
	  categorizeBy.var4 = 10
	  categorizeBy.var5 = 10
	  categorizeBy.var6 = 10
	  categorizeBy.var7 = 10
	  categorizeBy.var8 = 10
	  categorizeBy.var9 = 10
	  categorizeBy.var10 = 10
	  categorizeBy.var11 = 10
	  categorizeBy.var12 = 10
	  categorizeBy.var13 = 10
	  categorizeBy.var14 = 10
	  categorizeBy.var15 = 10
	  categorizeBy.var16 = 10
	  categorizeBy.var17 = 10
	  categorizeBy.var18 = 10
	  categorizeBy.var19 = 10
	  categorizeBy.var20 = 10
	  j.categorizeBy(categorizeBy)
	  
	  condition1 = j.selectWithName("name1")(0)
	  condition2 = j.selectWithName("name2")(0)
	  
	  //println ("C1 = "+condition1)
	  //println ("C2 = "+condition2)
	  
	  assert(condition1.var1==0)
	  assert(condition1.var2==0)
	  assert(condition1.var3==0)
	  assert(condition1.var4==0)
	  assert(condition1.var5==0)
	  assert(condition1.var6==0)
	  assert(condition1.var7==0)
	  assert(condition1.var8==0)
	  assert(condition1.var9==0)
	  assert(condition1.var10==0)
	  assert(condition1.var11==0)
	  assert(condition1.var12==0)
	  assert(condition1.var13==0)
	  assert(condition1.var14==0)
	  assert(condition1.var14==0)
	  assert(condition1.var15==0)
	  assert(condition1.var16==0)
	  assert(condition1.var17==0)
	  assert(condition1.var18==0)
	  assert(condition1.var19==0)
	  assert(condition1.var20==0)
	  
	  assert(condition2.var1==10)
	  assert(condition2.var2==10)
	  assert(condition2.var3==10)
	  assert(condition2.var4==10)
	  assert(condition2.var5==10)
	  assert(condition2.var6==10)
	  assert(condition2.var7==10)
	  assert(condition2.var8==10)
	  assert(condition2.var9==10)
	  assert(condition2.var10==10)
	  assert(condition2.var11==10)
	  assert(condition2.var12==10)
	  assert(condition2.var13==10)
	  assert(condition2.var14==10)
	  assert(condition2.var15==10)
	  assert(condition2.var16==10)
	  assert(condition2.var17==10)
	  assert(condition2.var18==10)
	  assert(condition2.var19==10)
	  assert(condition2.var20==10)
	  var min = j.getMinValues
	  var max = j.getMaxValues
	  println("MIN = "+min)
	  assert(min.var1==0.1f)
	  assert(min.var2==0.1f)
	  assert(min.var3==0.1f)
	  assert(min.var4==0.1f)
	  assert(min.var5==0.1f)
	  assert(min.var6==0.1f)
	  assert(min.var7==0.1f)
	  assert(min.var8==0.1f)
	  assert(min.var9==0.1f)
	  assert(min.var10==0.1f)
	  assert(min.var11==0.1f)
	  assert(min.var12==0.1f)
	  assert(max.var1==0.2f)
	  assert(max.var2==0.2f)
	  assert(max.var3==0.2f)
	  assert(max.var4==0.2f)
	  assert(max.var5==0.2f)
	  assert(max.var6==0.2f)
	  assert(max.var7==0.2f)
	  assert(max.var8==0.2f)
	  assert(max.var9==0.2f)
	  assert(max.var10==0.2f)
	  assert(max.var11==0.2f)
	  assert(max.var12==0.2f)
	  assert(j.getCategorizedBy.isEqual(categorizeBy))
	  var c = new Condition()
	  c.var1 = 0.15f
	  c.var2 = 0.15f
	  c.var3 = 0.15f
	  c.var4 = 0.15f
	  c.var5 = 0.15f
	  c.var6 = 0.15f
	  c.var7 = 0.15f
	  c.var8 = 0.15f
	  c.var9 = 0.15f
	  c.var10 = 0.15f
	  c.var11 = 0.15f
	  c.var12 = 0.15f
	  j.categorizeCondition(c)
	  assert(c.var1==5)
	  assert(c.var2==5)
	  assert(c.var3==5)
	  assert(c.var4==5)
	  assert(c.var5==5)
	  assert(c.var6==5)
	  assert(c.var7==5)
	  assert(c.var8==5)
	  assert(c.var9==5)
	  assert(c.var10==5)
	  assert(c.var11==5)
	  assert(c.var12==5)
	  
	}
    
    
}
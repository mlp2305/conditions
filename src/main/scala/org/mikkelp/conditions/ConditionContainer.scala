package org.mikkelp.conditions
import net.mikkelp.properties._

/**
 * Created with IntelliJ IDEA.
 * User: mikkel
 * Date: 25-08-13
 * Time: 18:59
 * To change this template use File | Settings | File Templates.
 */
class ConditionContainer extends  Serializable with Properties {
  
  var conditions:scala.collection.mutable.ArrayBuffer[Condition] = new scala.collection.mutable.ArrayBuffer[Condition]()

  def getConditions()  = {
    conditions.toList
  }
  
  def addCondition(c:Condition) {
    conditions.append(c)
  }

}

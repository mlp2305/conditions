
package org.mikkelp.conditions
import java.sql.DriverManager
import org.mikkelp.sql._
import scala.collection.JavaConversions._
import scala.collection.mutable.ArrayBuffer
import java.sql.Connection
abstract class ConditionDatabase(val newDb: Boolean) {
  val tableName = "conditions"
  val systemTableName = "system_conditions"
  protected var minCondition: Condition = null
  protected var maxCondition: Condition = null
  protected var categorizedByCondition: Condition = null;
  init();
  protected def init() {
    if (newDb)
      createDatabase()
  }

  def size() = {
    extractCount("select count(*) c from conditions");
  }
  def setProperty(name: String, value: String) {
    val i = new InsertStatement("user_properties");
    i.addField("name", name, true)
    i.addField("value", value, true)
    executeUpdate(i.toString())
    getConnection.commit()

  }
  def getProperty(name: String) = {
    val sql = "select * from user_properties where name='" + name + "'"
    val rs = executeQuery(sql);
    if (rs.next()) {
      rs.getString("value");
    } else {
      null
    }
  }
  def categorizeCondition(c: Condition): Condition = {

    categorizeCondition(c, getCategorizedBy)
    c
  }

  private def categorizeCondition(c: Condition, categorizeBy: Condition): Unit = {
    minCondition = getMinValues;
    maxCondition = getMaxValues
    c.var1 = math.round(categorizeBy.var1 * ((c.var1 - minCondition.var1) / (maxCondition.var1 - (minCondition.var1))))
    c.var2 = math.round(categorizeBy.var2 * ((c.var2 - minCondition.var2) / (maxCondition.var2 - minCondition.var2)))
    c.var3 = math.round(categorizeBy.var3 * ((c.var3 - minCondition.var3) / (maxCondition.var3 - minCondition.var3)))
    c.var4 = math.round(categorizeBy.var4 * ((c.var4 - minCondition.var4) / (maxCondition.var4 - minCondition.var4)))
    c.var5 = math.round(categorizeBy.var5 * ((c.var5 - minCondition.var5) / (maxCondition.var5 - minCondition.var5)))
    c.var6 = math.round(categorizeBy.var6 * ((c.var6 - minCondition.var6) / (maxCondition.var6 - minCondition.var6)))
    c.var7 = math.round(categorizeBy.var7 * ((c.var7 - minCondition.var7) / (maxCondition.var7 - minCondition.var7)))
    c.var8 = math.round(categorizeBy.var8 * ((c.var8 - minCondition.var8) / (maxCondition.var8 - minCondition.var8)))
    c.var9 = math.round(categorizeBy.var9 * ((c.var9 - minCondition.var9) / (maxCondition.var9 - minCondition.var9)))
    c.var10 = math.round(categorizeBy.var10 * ((c.var10 - minCondition.var10) / (maxCondition.var10 - minCondition.var10)))
    c.var11 = math.round(categorizeBy.var11 * ((c.var11 - minCondition.var11) / (maxCondition.var11 - minCondition.var11)))
    c.var12 = math.round(categorizeBy.var12 * ((c.var12 - minCondition.var12) / (maxCondition.var12 - minCondition.var12)))
    c.var13 = math.round(categorizeBy.var13 * ((c.var13 - minCondition.var13) / (maxCondition.var13 - minCondition.var13)))
    c.var14 = math.round(categorizeBy.var14 * ((c.var14 - minCondition.var14) / (maxCondition.var14 - minCondition.var14)))
    c.var15 = math.round(categorizeBy.var15 * ((c.var15 - minCondition.var15) / (maxCondition.var15 - minCondition.var15)))
    c.var16 = math.round(categorizeBy.var16 * ((c.var16 - minCondition.var16) / (maxCondition.var16 - minCondition.var16)))
    c.var17 = math.round(categorizeBy.var17 * ((c.var17 - minCondition.var17) / (maxCondition.var17 - minCondition.var17)))
    c.var18 = math.round(categorizeBy.var18 * ((c.var18 - minCondition.var18) / (maxCondition.var18 - minCondition.var18)))
    c.var19 = math.round(categorizeBy.var19 * ((c.var19 - minCondition.var19) / (maxCondition.var19 - minCondition.var19)))
    c.var20 = math.round(categorizeBy.var20 * ((c.var20 - minCondition.var20) / (maxCondition.var20 - minCondition.var20)))
    c.var21 = math.round(categorizeBy.var21 * ((c.var21 - minCondition.var21) / (maxCondition.var21 - minCondition.var21)))
  }

  def getMinValues(): Condition = {
    if (minCondition == null) {
      minCondition = selectFromSystemTable("_minValue")(0)
    }
    minCondition;
  }
  def getMaxValues(): Condition = {
    if (maxCondition == null) {
      maxCondition = selectFromSystemTable("_maxValue")(0)
    }
    maxCondition;
  }

  def getCategorizedBy() = {
    if (categorizedByCondition == null) {
      categorizedByCondition = selectFromSystemTable("_categorizedBy")(0);
    }
    categorizedByCondition
  }
  protected def dropDatabase() {
    dropTable(tableName)
    dropTable(systemTableName)
    dropTable("user_properties")
  }

  protected def dropTable(name: String) {
    try {
      execute("drop table " + name)
    } catch {
      case t => // todo: handle error
    }
  }

  protected def extractCount(sql: String): Integer = {
    val st = executeQuery(sql);
    if (st.next)
      st.getInt("c");
    else
      0

  }
  protected def createDatabase() {
    dropDatabase()
    createTables()
    createIndexes();
  }

  protected def createConditionTable(name: String) {
    //var sql = "create table "+name+" (id int GENERATED BY DEFAULT AS IDENTITY (START WITH 1 INCREMENT BY 1) not null, constraint DID primary key (id),name VARCHAR(50),count_tokens FLOAT NOT NULL,";
    var sql = "create table " + name + " (id int GENERATED BY DEFAULT AS IDENTITY (START WITH 1 INCREMENT BY 1) not null, name VARCHAR(100),tag VARCHAR(100), count_tokens INT NOT NULL,";
    sql = sql + "success BOOLEAN,success_count INT not null,var1 FLOAT NOT NULL,var2 FLOAT NOT NULL,var3 FLOAT NOT NULL,var4 FLOAT NOT NULL,var5 FLOAT NOT NULL,var6 FLOAT NOT NULL,var7 FLOAT NOT NULL"
    sql = sql + ",var8 FLOAT NOT NULL,var9 FLOAT NOT NULL,var10 FLOAT NOT NULL,var11 FLOAT NOT NULL,var12 FLOAT NOT NULL"
    sql = sql + ",var13 FLOAT NOT NULL,var14 FLOAT NOT NULL,var15 FLOAT NOT NULL,var16 FLOAT NOT NULL,var17 FLOAT NOT NULL,var18 FLOAT NOT NULL,var19 FLOAT NOT NULL,var20 FLOAT NOT NULL, var21 FLOAT NOT NULL)"
    execute(sql);
  }

  protected def createTables() {
    createConditionTable(tableName)
    createConditionTable(systemTableName)
    createPropertiesTable();

  }
  protected def createPropertiesTable() {
    val sql = "create table user_properties (name VARCHAR(1000) primary key not null, value varchar(1000))";
    execute(sql)
  }

  protected def createInsertStatement(tableName: String) = {
    new InsertStatement(tableName);
  }

  protected def selectMax(varName: String) = {
    val st = executeQuery("select max(" + varName + ") m from " + tableName);
    if (st.next()) {
      st.getFloat("m")
    } else {
      -1
    }

  }
  def round(a: Float) = {
    math.round(a * 100) / 100;
  }
  def selectMin(varName: String) = {
    val st = executeQuery("select min(" + varName + ") m from " + tableName);
    if (st.next()) {
      st.getFloat("m")
    } else {
      -1
    }

  }
  def insertConditions(conditions: List[Condition]) = conditions.foreach(insertCondition(_))

  def insertCondition(c: Condition) {
    insertIntoTable(c, tableName)
  }

  def selectWithName(name: String): List[Condition] = {
    selectWithName(name, tableName)
  }

  def selectFromSql(sql: String) = {
    selectFromSqlWithIds(sql).map(x => x._2)
  }
  def selectAll() = {
    selectFromSql("select * from " + tableName)
  }

  def categorizeBy(c: Condition) {

    setMinMaxValues()
    saveMinMaxValues
    c.name = "_categorizedBy"
    insertIntoSystemTable(c)
    categorizeVar("var1", c.var1)
    categorizeVar("var2", c.var2)
    categorizeVar("var3", c.var3)
    categorizeVar("var4", c.var4)
    categorizeVar("var5", c.var5)
    categorizeVar("var6", c.var6)
    categorizeVar("var7", c.var7)
    categorizeVar("var8", c.var8)
    categorizeVar("var9", c.var9)
    categorizeVar("var10", c.var10)
    categorizeVar("var11", c.var11)
    categorizeVar("var12", c.var12)
    categorizeVar("var13", c.var13)
    categorizeVar("var14", c.var14)
    categorizeVar("var15", c.var15)
    categorizeVar("var16", c.var16)
    categorizeVar("var17", c.var17)
    categorizeVar("var18", c.var18)
    categorizeVar("var19", c.var19)
    categorizeVar("var20", c.var20)
    categorizeVar("var21", c.var21)

  }

  def getNumberOfConditionWithSameState(c: Condition) = {
    extractCount("select count(*) c from conditions where " + createCompareStateSql(c))
  }
  def getNumberOfConditionThatEquals(c: Condition, extra: String = ""): Int = {
    var sql = "select count(*) c from conditions where " + createCompareEqualsSql(c) + " " + extra
    extractCount(sql)
  }

  protected def insertIntoTable(c: Condition, tableName: String) {
    var i = createInsertStatement(tableName)
    c.count = 0
    i.addField("name", c.name, true)
    i.addField("tag", c.tag, true)
    i.addField("success_count", c.successCount, true)
    i.addField("success", c.isSuccess, false);
    i.addField("count_tokens", c.count, false);
    i.addField("var1", c.var1, false);
    i.addField("var2", c.var2, false);
    i.addField("var3", c.var3, false);
    i.addField("var4", c.var4, false);
    i.addField("var5", c.var5, false);
    i.addField("var6", c.var6, false);
    i.addField("var7", c.var7, false);
    i.addField("var8", c.var8, false);
    i.addField("var9", c.var9, false);
    i.addField("var10", c.var10, false);
    i.addField("var11", c.var11, false);
    i.addField("var12", c.var12, false);
    i.addField("var13", c.var13, false);
    i.addField("var14", c.var14, false);
    i.addField("var15", c.var15, false);
    i.addField("var16", c.var16, false);
    i.addField("var17", c.var17, false);
    i.addField("var18", c.var18, false);
    i.addField("var19", c.var19, false);
    i.addField("var20", c.var20, false);
    i.addField("var21", c.var21, false);
    val sql = i.buildSql();
    executeUpdate(sql);
  }
  protected def selectFromSystemTable(name: String) = {
    selectWithName(name, systemTableName)
  }
  protected def selectWithName(name: String, table: String): List[Condition] = {
    val sql = "select * from " + table + " where name='" + name + "'";
    selectFromSql(sql);
  }
  protected def insertIntoSystemTable(c: Condition) {
    insertIntoTable(c, systemTableName)
  }
  protected def selectFromSqlWithIds(sql: String) = {
    val st = executeQuery(sql)

    var conditionList = new scala.collection.mutable.ArrayBuffer[(Integer, Condition)]
    while (st.next()) {
      val condition = new Condition();
      condition.count = st.getInt("count_tokens");
      condition.success = st.getBoolean("success");
      condition.successCount = st.getInt("success_count");
      condition.name = st.getString("name");
      condition.var1 = st.getFloat("var1")
      condition.var2 = st.getFloat("var2")
      condition.var3 = st.getFloat("var3")
      condition.var4 = st.getFloat("var4")
      condition.var5 = st.getFloat("var5")
      condition.var6 = st.getFloat("var6")
      condition.var7 = st.getFloat("var7")
      condition.var8 = st.getFloat("var8")
      condition.var9 = st.getFloat("var9")
      condition.var10 = st.getFloat("var10")
      condition.var11 = st.getFloat("var11")
      condition.var12 = st.getFloat("var12")
      condition.var13 = st.getFloat("var13")
      condition.var14 = st.getFloat("var14")
      condition.var15 = st.getFloat("var15")
      condition.var16 = st.getFloat("var16")
      condition.var17 = st.getFloat("var17")
      condition.var18 = st.getFloat("var18")
      condition.var19 = st.getFloat("var19")
      condition.var20 = st.getFloat("var20")
      condition.var21 = st.getFloat("var21")
      conditionList += ((st.getInt("id"), condition))

    }
    st.close()
    conditionList.toList

  }

  protected def saveMinMaxValues() {
    insertIntoSystemTable(minCondition)
    insertIntoSystemTable(maxCondition)
  }
  protected def categorizeVar(varName: String, categorizeBy: Float) = {
    val min = selectMin(varName)
    val max = selectMax(varName)
    println("VAR NAME = " + varName + " min = " + min + " max = " + max)
    var maxMinusMin = max - min
    if (max - min != 0) {
      var sql = "update " + tableName + " set varX = round(categorizeBy*((varX-(min))/(thespan)))"

      sql = sql.replaceAll("varX", varName)
      sql = sql.replaceAll("min", "" + min)
      sql = sql.replaceAll("max", "" + max)
      sql = sql.replaceAll("categorizeBy", "" + categorizeBy)
      sql = sql.replaceAll("thespan", "" + maxMinusMin)
      println(sql)
      executeUpdate(sql)

    }
  }

  def deleteWithNoneSuccess() {
    var deleteSql = "delete from " + tableName + " where success_count<1"
    println("DELETE SQL " + deleteSql)
    executeUpdate(deleteSql);
  }

  var zipped = 0;
  def zip() {
    var count = 1;
    while (zipNext) {
      println("size = "+size + " zipped = " + zipped)
      count = count + 1
      getConnection.commit()
    }
    //deleteWithNoneSuccess

  }
  protected def zipNext() = {
    val sql = "select * from conditions where count_tokens=0 limit 1";
    var conditions = selectFromSqlWithIds(sql);
    if (!conditions.isEmpty) {
      var ct = conditions(0)
      var id = ct._1
      var condition = ct._2
      var totalCount = getNumberOfConditionThatEquals(condition)
      var successCount = getNumberOfConditionThatEquals(condition, " and success=true")
      //println("SUCCESS COUNT= "+successCount+" total count="+totalCount)

      if (totalCount == 1 & (!condition.isSuccess)) {
        deleteWithId(id)
        zipped = zipped + 1
      } else {
        if(totalCount==1) {
          val updateStatement = new UpdateStatement(tableName)
          updateStatement.addField("count_tokens", "" + 1, false)
          updateStatement.addField("success_count", "" + 1, false)
          updateStatement.setWhereClause(" where id=" + id)
          executeUpdate(updateStatement.buildSql())
        }else
        if (totalCount > 1) {
          zipped = zipped+totalCount
          val deleteSql = "delete from conditions where " + createCompareEqualsSql(condition) + " and id<>" + id
          executeUpdate(deleteSql)
          //println(deleteSql)
          val updateStatement = new UpdateStatement(tableName)
          updateStatement.addField("count_tokens", "" + totalCount, false)
          updateStatement.addField("success_count", "" + successCount, false)
          updateStatement.setWhereClause(" where id=" + id)
          executeUpdate(updateStatement.buildSql())
        }
      }
      true
    } else {
      false
    }

  }
  def deleteWithId(id: Int) {
    executeUpdate("delete from conditions where id=" + id)
  }
  def selectWhenSameState(c: Condition, limit: Int = 0) = {
    var sql = "select * from conditions where " + createCompareStateSql(c);
    if (limit > 0) {
      sql = sql + " limit " + limit
    }
    selectFromSql(sql);
  }
  def selectWhenSameStateOrderBySuccessRateDesc(c: Condition, min_success_count: Int, success_rate: Float, limit: Int) = {
    //var sql = "select * from conditions where success_count>=" + min_success_count + "  and " + createCompareStateSql(c) + " order by (success_count/count_tokens),success_count desc limit " + limit
    var sql = "select * from conditions where success_count>=" + min_success_count + "  and " + createCompareStateSql(c) + " order by success_count desc limit " + limit
    selectFromSql(sql);
  }
  def selectWhenSameStateOrderBySuccessRateAsc(c: Condition, min_success_count: Int, success_rate: Float, limit: Int) = {
    var sql = "select * from conditions where success_count>=" + min_success_count + "  and " + createCompareStateSql(c) + " order by success_count asc limit " + limit
    selectFromSql(sql);
  }
  def selectWhenEquals(c: Condition, extra: String = "") = {
    val sql = "select * from conditions where name='" + c.name + "' and " + createCompareStateSql(c) + " " + extra
    selectFromSql(sql);
  }

  protected def createCompareEqualsSql(c: Condition) = {
    "name='" + c.name + "' and " + createCompareStateSql(c);
  }

  protected def createCompareStateSql(c: Condition) = {
    var sql = " var1=" + c.var1 +
      " and var2=" + c.var2 +
      " and var3=" + c.var3 +
      " and var4=" + c.var4 +
      " and var5=" + c.var5 +
      " and var6=" + c.var6 +
      " and var7=" + c.var7 +
      " and var8=" + c.var8 +
      " and var9=" + c.var9 +
      " and var10=" + c.var10 +
      " and var11=" + c.var11 +
      " and var12=" + c.var12 +
      " and var13=" + c.var13 +
      " and var14=" + c.var14 +
      " and var15=" + c.var15 +
      " and var16=" + c.var16 +
      " and var17=" + c.var17 +
      " and var18=" + c.var18 +
      " and var19=" + c.var19 +
      " and var20=" + c.var20 +
      " and var21=" + c.var21
    sql
  }

  protected def createIndexes() {
    var sql = "CREATE INDEX i_var1 ON conditions(var1,var2,var3,var4,var5,var6,var7,var8,var9,var10,var11,var12,var13,var14,var15,var16,var17,var18,var19,var20, var21)"
    execute(sql);
  }

  def executeQuery(sql: String) = {
    var st = getStatement()
    var r = st.executeQuery(sql)
    st.close()
    r
  }

  def getConnection(): Connection

  protected def executeUpdate(sql: String) {
    val st = getStatement;
    st.executeUpdate(sql)
    st.close()
    //getConnection.commit()

  }
  protected def execute(sql: String) {
    val st = getConnection.createStatement();
    //println("****")
    //println(sql)
    st.execute(sql)
    st.close()
  }
  protected def getStatement() = {
    getConnection().createStatement();
  }

  protected def setMinMaxValues() {
    minCondition = new Condition()
    maxCondition = new Condition()
    minCondition.name = "_minValue"
    maxCondition.name = "_maxValue"

    minCondition.var1 = selectMin("var1")
    minCondition.var2 = selectMin("var2")
    minCondition.var3 = selectMin("var3")
    minCondition.var4 = selectMin("var4")
    minCondition.var5 = selectMin("var5")
    minCondition.var6 = selectMin("var6")
    minCondition.var7 = selectMin("var7")
    minCondition.var8 = selectMin("var8")
    minCondition.var9 = selectMin("var9")
    minCondition.var10 = selectMin("var10")
    minCondition.var11 = selectMin("var11")
    minCondition.var12 = selectMin("var12")
    minCondition.var13 = selectMin("var13")
    minCondition.var14 = selectMin("var14")
    minCondition.var15 = selectMin("var15")
    minCondition.var16 = selectMin("var16")
    minCondition.var17 = selectMin("var17")
    minCondition.var18 = selectMin("var18")
    minCondition.var19 = selectMin("var19")
    minCondition.var20 = selectMin("var20")
    minCondition.var21 = selectMin("var21")
    maxCondition.var1 = selectMax("var1")
    maxCondition.var2 = selectMax("var2")
    maxCondition.var3 = selectMax("var3")
    maxCondition.var4 = selectMax("var4")
    maxCondition.var5 = selectMax("var5")
    maxCondition.var6 = selectMax("var6")
    maxCondition.var7 = selectMax("var7")
    maxCondition.var8 = selectMax("var8")
    maxCondition.var9 = selectMax("var9")
    maxCondition.var10 = selectMax("var10")
    maxCondition.var11 = selectMax("var11")
    maxCondition.var12 = selectMax("var12")
    maxCondition.var13 = selectMax("var13")
    maxCondition.var14 = selectMax("var14")
    maxCondition.var15 = selectMax("var15")
    maxCondition.var16 = selectMax("var16")
    maxCondition.var17 = selectMax("var17")
    maxCondition.var18 = selectMax("var18")
    maxCondition.var19 = selectMax("var19")
    maxCondition.var20 = selectMax("var20")
    maxCondition.var21 = selectMax("var21")
  }
}

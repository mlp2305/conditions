package org.mikkelp.conditions
import java.io.{FileOutputStream, File, FileInputStream}

/**
 * Created with IntelliJ IDEA.
 * User: mikkel
 * Date: 27-08-13
 * Time: 21:34
 * To change this template use File | Settings | File Templates.
 */
class ConditionContainerLoader(val file:String) {

  def load() = {
    var g:ConditionContainer = null
    try {
      var s = new java.io.ObjectInputStream(new FileInputStream(new File(file)))
      g = s.readObject().asInstanceOf[ConditionContainer]
      println("Game conditions loaded "+g.conditions.size)
    }catch {
      case e:Exception=>{
        e.printStackTrace();
        g = new ConditionContainer()
        println("Game conditions not loaded")
      }
    }
    g
  }
  def save(g:ConditionContainer) {
    try {
      var s = new java.io.ObjectOutputStream(new FileOutputStream(new File(file)))
      s.writeObject(g)
      s.flush()
      s.close()
    }catch {
      case e:Exception=>{
        e.printStackTrace()
      }
    }

  }
}

package org.mikkelp.conditions

/**
 * Created with IntelliJ IDEA.
 * User: mikkel
 * Date: 25-08-13
 * Time: 15:49
 * To change this template use File | Settings | File Templates.
 */
object ConditionListPrinter {
  def print(conditions:List[Condition]) {
    conditions.foreach(printCondition(_))
   }
  def printCondition(c:Condition) {
    println("********")
    println(c)
    println("********")
  }
}

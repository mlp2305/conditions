package org.mikkelp.conditions
import java.sql.DriverManager
import java.sql.Connection
class HSQLDBResources(val fileName: String, val newdb: Boolean) extends ConditionDatabase(newdb) {
  lazy val connection: Connection = DriverManager.getConnection("jdbc:hsqldb:res:" + fileName, "sa", "")
  def getConnection(): Connection = connection
}

class HSQLDBFile(val fileName: String, val newdb: Boolean) extends ConditionDatabase(newdb) {
  lazy val connection: Connection = DriverManager.getConnection("jdbc:hsqldb:file:" + fileName, "sa", "")
  def getConnection(): Connection = connection
}

object TestHsqldbFile extends App {
  val db = new HSQLDBResources("c:/test/ob3d/punchdatabase.db", false)
  var r = db.selectAll;
}

class HSQLDBMemory(val name:String) extends ConditionDatabase(true) {
	lazy val connection = DriverManager.getConnection("jdbc:hsqldb:mem:"+System.currentTimeMillis(), "sa", "")
    def getConnection() = connection
}

package org.mikkelp.conditions

/**
 * Created with IntelliJ IDEA.
 * User: mikkel
 * Date: 25-08-13
 * Time: 12:30
 * To change this template use File | Settings | File Templates.
 */
class Condition extends Serializable {
  var name = ""
  var tag = ""
  var a = 0f;
  var b = 0f;
  var c = 0f;
  var d = 0f;
  var e = 0f;
  var f = 0f;
  var g = 0f;
  
  var var1 = 0f
  var var2 = 0f
  var var3 = 0f;
  var var4 = 0f;
  var var5 = 0f;
  var var6 = 0f;
  var var7 = 0f;
  var var8 = 0f;
  var var9 = 0f;
  var var10 = 0f;
  var var11 = 0f;
  var var12 = 0f;
  var var13 = 0f;
  var var14 = 0f;
  var var15 = 0f;
  var var16 = 0f;
  var var17 = 0f;
  var var18 = 0f;
  var var19 = 0f;
  var var20 = 0f;
  var var21 = 0f;
  var var22 = 0f;
  var var23 = 0f;
  var var24 = 0f;
  var var25 = 0f;
  var var26 = 0f;
  var var27 = 0f;
  var var28 = 0f;
  var var29 = 0f;
  var var30 = 0f;

  var count = 1
  var successCount = 0;
  var success = false

  def successRate = successCount / count
  def incSuccess() {
    successCount = successCount + 1
  }
  def isSuccess() = success
  def setSucces(b: Boolean) {
    success = b
  }
  def copyFrom(c: Condition) = {
    name = c.name;
    tag = c.tag
    var1 = c.var1
    var2 = c.var2;
    var3 = c.var3
    var4 = c.var4
    var5 = c.var5
    var6 = c.var6
    var7 = c.var7
    var8 = c.var8
    var9 = c.var9
    var10 = c.var10
    var11 = c.var11
    var12 = c.var12
    var13 = c.var13
    var14 = c.var14
    var15 = c.var15
    var16 = c.var16
    var17 = c.var17
    var18 = c.var18
    var19 = c.var19
    var20 = c.var20
    var21 = c.var21
    count = c.count
    successCount = c.successCount
    success = c.success
    this

  }
  override def toString() = {
    var str = name + " var1=" + var1 + " var2 = " + var2 + " var3 = " + var3 + " var4 = " + var4 + "  var5=" + var5 + " var6  = " + var6 + " var7 = " + var7 + " var8 = " + var8
    str = str + "var9 = " + var9 + " var10 =" + var10 + " var11=" + var11 + " var12 = " + var12
    str

  }
  def copy() = {
    val c = newCondition();
    c.name = name;
    c.tag = tag;
    c.count = count
    c.success = success
    c.var1 = var1
    c.var2 = var2;
    c.var3 = var3
    c.var4 = var4
    c.var5 = var5
    c.var6 = var6
    c.var7 = var7
    c.var8 = var8
    c.var9 = var9
    c.var10 = var10
    c.var11 = var11
    c.var12 = var12
    c.var13 = var13
    c.var14 = var14
    c.var15 = var15
    c.var16 = var16
    c.var17 = var17
    c.var18 = var18
    c.var19 = var19
    c.var20 = var20
    c.var21 = var21
    c
  }
  def newCondition() = {
    new Condition();
  }

  def incrementCount() {
    count = count + 1
  }

  def hasSameState(c: Condition) = {

    var1 == c.var1 & var2 == c.var2 & var3 == c.var3 & var4 == c.var4 & var5 == c.var5 & var6 == c.var6 & var7 == c.var7 & var8 == c.var8 & var9 == c.var9 & var10 == c.var10 & var11 == c.var11 & var12 == c.var12 &
      var13 == c.var13 & var14 == c.var14 & var15 == c.var15 & var16 == c.var16 & var17 == c.var17 & var18 == c.var18 & var19 == c.var19 & var20 == c.var20 & var21 == c.var21
  }
  def isEqual(c: Condition) = {
    name == c.name && hasSameState(c)

  }

}


